import * as THREE from 'three';
import TWEEN from 'three/addons/libs/tween.module.js';
import { TrackballControls } from 'three/addons/controls/TrackballControls.js';

let imageWidth = 512;
let imageHeight = 288;
let containerAspect = 3;
let camera, scene, renderer;
let controls;

function element(elem, w, h, x, y, z, ry) {
  const geometry = new THREE.PlaneGeometry(w, h);
  let textureOpts = { side:THREE.DoubleSide };
  if (elem.getAttribute("data-src")) {
    const src = elem.getAttribute("data-src");
    const texture = (new THREE.TextureLoader()).load(src);
    textureOpts = { ...textureOpts, color: 0xeeeeee, map:texture };
  }
  const material = new THREE.MeshBasicMaterial(textureOpts);
  const mesh = new THREE.Mesh(geometry, material);
  mesh.position.set(x, y, z);
  mesh.rotation.y = ry;
  return mesh;
}

function init() {
  const container = document.getElementById("container");
  const width = window.innerWidth;
  const height = width/containerAspect;
  camera = new THREE.PerspectiveCamera(50, width/height, 1, 5000);
  camera.position.set(0, 200, 700);
  scene = new THREE.Scene();
  if (false) {
    scene.background = new THREE.Color(0x88ccff);
    scene.fog = new THREE.Fog(0x88ccff, 10, 5000);
    const geometry = new THREE.PlaneGeometry(10000, 10000);
    const material = new THREE.MeshBasicMaterial({ color:0x0e2f92, side:THREE.DoubleSide });
    const mesh = new THREE.Mesh(geometry, material);
    mesh.position.set(0, -600, 0);
    mesh.rotation.x = Math.PI/2;
    scene.add(mesh);
  } else {
    const loader = new THREE.CubeTextureLoader();
    loader.setPath( '../textures/cube/Bridge2/' );
    const textureCube = loader.load( [ 'posx.jpg', 'negx.jpg', 'posy.jpg', 'negy.jpg', 'posz.jpg', 'negz.jpg' ] );
    const textureLoader = new THREE.TextureLoader();
    const textureEquirec = textureLoader.load( 'textures/2294472375_24a3b8ef46_o.jpg' );
    textureEquirec.mapping = THREE.EquirectangularReflectionMapping;
    textureEquirec.colorSpace = THREE.SRGBColorSpace;
    scene.background = textureCube;
  }
  renderer = new THREE.WebGLRenderer();
  renderer.setSize(width, height);
  container.appendChild(renderer.domElement);
  const images = document.getElementById("images");
  if (images) {
    const group = new THREE.Group();
    group.add(element(images.children[0], imageWidth, imageHeight, 0, 0, imageWidth/2, 0));
    group.add(element(images.children[1], imageWidth, imageHeight, imageWidth/2, 0, 0, Math.PI/2));
    group.add(element(images.children[2], imageWidth, imageHeight, 0, 0, -imageWidth/2, Math.PI));
    group.add(element(images.children[3], imageWidth, imageHeight, -imageWidth/2, 0, 0, -Math.PI/2));
    scene.add(group);
  }
  if (false) {
    const geometry = new THREE.BoxGeometry(50, 50, 50);
    const material = new THREE.MeshBasicMaterial({ color:0x880000, side:THREE.DoubleSide });
    const mesh = new THREE.Mesh(geometry, material);
    mesh.position.set(0, 300, 0);
    mesh.rotation.x = Math.PI/2;
    scene.add(mesh);
  }
  controls = new TrackballControls(camera, renderer.domElement);
  controls.rotateSpeed = 4;
  window.addEventListener('resize', onWindowResize);
}

function onWindowResize() {
  const width = window.innerWidth;
  const height = width/containerAspect;
  camera.aspect = width/height;
  camera.updateProjectionMatrix();
  renderer.setSize(width, height);
}

function easeIn(x, y, z, wait, duration) {
  return new TWEEN.Tween(camera.position)
    .to({ x, y, z }, duration)
    .delay(wait)
    .easing(TWEEN.Easing.Exponential.In)
    .onStart(function () {
      controls.reset();
    });
}

function easeOut(x, y, z, duration) {
  return new TWEEN.Tween(camera.position)
    .to({ x, y, z }, duration)
    .easing(TWEEN.Easing.Cubic.Out);
}

function rand(min, max) {
  return Math.random()*(max-min)+min;
}

function tween() {
  const d1 = 500;
  const d2 = 500;
  const wait = 3000;
  const zoomInDist = 600;
  const zoomOutDist = 1200;
  const t = [];
  t.push(easeOut(0, 0, zoomInDist, wait, d1));
  t.push(easeIn(zoomOutDist, rand(-500,500), 0, d2));
  t.push(easeOut(zoomInDist, 0, 0, wait, d1));
  t.push(easeIn(0, rand(-500,500), -zoomOutDist, d2));
  t.push(easeOut(0, 0, -zoomInDist, wait, d1));
  t.push(easeIn(-zoomOutDist, rand(-500,500), 0, d2));
  t.push(easeOut(-zoomInDist, 0, 0, wait, d1));
  t.push(easeIn(0, rand(-500,500), zoomOutDist, d2));
  for (let i = 0; i < t.length-1; i++) {
    t[i].chain(t[i+1]);
  }
  t[t.length-1].onComplete(tween);
  t[0].start();
}

function animate() {
  requestAnimationFrame(animate);
  TWEEN.update();
  controls.update();
  renderer.render(scene, camera);
}

TWEEN.removeAll();
init();
tween();
animate();
