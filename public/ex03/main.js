import * as THREE from 'three';

import { TrackballControls } from 'three/addons/controls/TrackballControls.js';
import { CSS3DRenderer, CSS3DObject } from 'three/addons/renderers/CSS3DRenderer.js';

let imageWidth = 512;
let imageHeight = 288;
let containerAspect = 3;
let camera, scene, renderer;
let controls;

function element(elem, x, y, z, ry) {
  elem.style.width = imageWidth + "px";
  elem.style.height = imageHeight + "px";
  elem.style.border = "0px";
  const obj = new CSS3DObject(elem);
  obj.position.set(x, y, z);
  obj.rotation.y = ry;
  return obj;
}

function init() {
  const container = document.getElementById('container');
  const width = window.innerWidth;
  const height = width/containerAspect;
  camera = new THREE.PerspectiveCamera(50, width/height, 1, 5000);
  camera.position.set(0, 200, 700);
  scene = new THREE.Scene();
  renderer = new CSS3DRenderer();
  renderer.setSize(width, height);
  container.appendChild(renderer.domElement);
  const images = document.getElementById("images");
  const group = new THREE.Group();
  group.add(element(images.children[0], 0, 0, imageWidth/2, 0));
  group.add(element(images.children[1], imageWidth/2, 0, 0, Math.PI/2));
  group.add(element(images.children[2], 0, 0, -imageWidth/2, Math.PI));
  group.add(element(images.children[3], -imageWidth/2, 0, 0, -Math.PI/2));
  scene.add(group);
  controls = new TrackballControls(camera, renderer.domElement);
  controls.rotateSpeed = 4;
  window.addEventListener('resize', onWindowResize);
}

function onWindowResize() {
  const width = window.innerWidth;
  const height = width/containerAspect;
  camera.aspect = width/height;
  camera.updateProjectionMatrix();
  renderer.setSize(width, height);
}

function animate() {
  requestAnimationFrame(animate);
  controls.update();
  renderer.render(scene, camera);
}

init();
animate();
