import * as THREE from 'three';
import TWEEN from 'three/addons/libs/tween.module.js';
import { TrackballControls } from 'three/addons/controls/TrackballControls.js';
import { CSS3DRenderer, CSS3DObject } from 'three/addons/renderers/CSS3DRenderer.js';

let imageWidth = 512;
let imageHeight = 288;
let containerAspect = 3;
let camera, scene, renderer;
let controls;

function element(elem, x, y, z, ry) {
  elem.style.width = imageWidth + "px";
  elem.style.height = imageHeight + "px";
  elem.style.border = "0px";
  const obj = new CSS3DObject(elem);
  obj.position.set(x, y, z);
  obj.rotation.y = ry;
  return obj;
}

function init() {
  const container = document.getElementById("container");
  const width = window.innerWidth;
  const height = width/containerAspect;
  camera = new THREE.PerspectiveCamera(50, width/height, 1, 5000);
  camera.position.set(0, 200, 700);
  scene = new THREE.Scene();
  renderer = new CSS3DRenderer();
  renderer.setSize(width, height);
  container.appendChild(renderer.domElement);
  const images = document.getElementById("images");
  const group = new THREE.Group();
  group.add(element(images.children[0], 0, 0, imageWidth/2, 0));
  group.add(element(images.children[1], imageWidth/2, 0, 0, Math.PI/2));
  group.add(element(images.children[2], 0, 0, -imageWidth/2, Math.PI));
  group.add(element(images.children[3], -imageWidth/2, 0, 0, -Math.PI/2));
  scene.add(group);

  const div = document.getElementById("floor");
  if (div) {
    const object = new CSS3DObject(div);
    object.position.set(0, -600, 0);
    object.rotation.x = Math.PI/2;
    scene.add(object);
  }

  controls = new TrackballControls(camera, renderer.domElement);
  controls.rotateSpeed = 4;
  window.addEventListener("resize", onWindowResize);
}

function onWindowResize() {
  const width = window.innerWidth;
  const height = width/containerAspect;
  camera.aspect = width/height;
  camera.updateProjectionMatrix();
  renderer.setSize(width, height);
}

function easeIn(x, y, z, wait, duration) {
  return new TWEEN.Tween(camera.position)
    .to({ x, y, z }, duration)
    .delay(wait)
    .easing(TWEEN.Easing.Exponential.In)
    .onStart(function () {
      controls.reset();
    });
}

function easeOut(x, y, z, duration) {
  return new TWEEN.Tween(camera.position)
    .to({ x, y, z }, duration)
    .easing(TWEEN.Easing.Cubic.Out);
}

function rand(min, max) {
  return Math.random()*(max-min)+min;
}

function tween() {
  const d1 = 500;
  const d2 = 500;
  const wait = 3000;
  const zoomInDist = 600;
  const zoomOutDist = 1200;
  const t = [];
  t.push(easeOut(0, 0, zoomInDist, wait, d1));
  t.push(easeIn(zoomOutDist, rand(-500,500), 0, d2));
  t.push(easeOut(zoomInDist, 0, 0, wait, d1));
  t.push(easeIn(0, rand(-500,500), -zoomOutDist, d2));
  t.push(easeOut(0, 0, -zoomInDist, wait, d1));
  t.push(easeIn(-zoomOutDist, rand(-500,500), 0, d2));
  t.push(easeOut(-zoomInDist, 0, 0, wait, d1));
  t.push(easeIn(0, rand(-500,500), zoomOutDist, d2));
  for (let i = 0; i < t.length-1; i++) {
    t[i].chain(t[i+1]);
  }
  t[t.length-1].onComplete(tween);
  t[0].start();
}

function animate() {
  requestAnimationFrame(animate);
  TWEEN.update();
  controls.update();
  renderer.render(scene, camera);
}

TWEEN.removeAll();
init();
tween();
animate();
